# Transmission routes P.A.

Data-augmented MCMC for estimating the transmission parameters and the relative contributions of endogenous route, cross-transmission and environmental contamination after discharge.